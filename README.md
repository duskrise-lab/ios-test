# DuskRise iOS Tech Test


**Welcome to the DuskRise iOS Tech Test**

Here you can find two projects:

 - A fully-operational app 🧐 (`DuskRiseTechTest` in `Project` folder)
 - Some technical questions 🤖 (`AlgorithmAndDataStructs` and `PracticalQuestions` in `QuestionsProjects` folder)

## QuestionsProjects

**AlgorithmAndDataStructs**
Just follow instructions you will find in the code and make it work, we will discuss it during the interview session 🤓

**PracticalQuestions**
Take a look on what we are asking. You can write code before or during the interview as you prefer, then we will talk about implementation during the interview.

## DuskRiseTechTest
It contains bugs, can crash unexpectedly, and has been built purposely avoiding best practices 🤪.
We want to use these “imperfections” to spark a discussion on the implementation of best practices, bug fixing and code improvements.

**Please make sure to go over it before your interview, prepare a list of key issues/drawbacks you have noticed, and come up with how you would prioritise fixing them.**

No need to improve the code or fix bugs in advance, we will do it together during the interview session 😉.
