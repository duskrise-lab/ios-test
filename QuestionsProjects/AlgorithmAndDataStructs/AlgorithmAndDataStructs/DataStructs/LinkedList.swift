//
//  LinkedList.swift
//  AlgorithmAndDataStructs
//
//  Created by Tereshkin Sergey on 23/11/22.
//

// MARK: max points 20

public class LinkedList {
    private var head: Node?
    private var tail: Node?

    public class Node {
        var next: Node?
        var prev: Node?

        let value: String

        public init(value: String) {
            self.value = value
        }
    }

    public var isEmpty: Bool {
        head == nil
    }

    public var first: Node? {
        head
    }

    public var last: Node? {
        tail
    }

    // MARK: points: 4
    public func append(value: String) {
        // TODO: implement append new node to list
    }

    // MARK: points: 8
    public func nodeAtIndex(_ index: Int) -> Node? {
        // TODO: implement node at index
        return nil
    }

    // MARK: points: 6
    public func remove(node: Node) -> String {
        // TODO: remove the node and return it's value
        return ""
    }

    // MARK: points: 2
    public func clear() {
        // TODO: implement remove all nodes from list
    }

    // MARK: debug function
    public func printNodeValueAt(index: Int) {
        debugPrint("Node.value at index \(index): \(nodeAtIndex(index)?.value ?? "nil")" )
    }
}

