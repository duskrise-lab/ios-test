//
//  ViewController.swift
//  AlgorithmAndDataStructs
//
//  Created by Tereshkin Sergey on 23/11/22.
//

import UIKit

class DRAlgorithmAndDataStructsTestController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        linkedListImplementation()
    }

    // MARK: ex. #1 - LinkedList max possible points for complete implementation 20
    // TODO: implement all missing methods inside LinkedList.swift file
    
    private func linkedListImplementation() {

        let testIndex = 6

        let l_list = LinkedList()

        for i in 0...12 {
            l_list.append(value: "value \(i)")
        }

        // print node at index
        l_list.printNodeValueAt(index: testIndex)

        // remove one node
        guard let nodeToRemove = l_list.nodeAtIndex(testIndex) else {
            return
        }

        debugPrint("Removing: Node at index \(testIndex): \(l_list.remove(node: nodeToRemove))" )

        // check if node was removed
        l_list.printNodeValueAt(index: testIndex)

        debugPrint("Removing all nodes..." )

        // remove all nodes
        l_list.clear()

        debugPrint("l_list.isEmpty: \(l_list.isEmpty)." )
    }
}
