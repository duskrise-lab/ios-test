//
//  ViewController.swift
//  PracticalQuestions
//
//  Created by Tereshkin Sergey on 23/11/22.
//

import UIKit

class DRPracticalTestController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // MARK: ex. #1 - Count Characters
        // count and print occurrences of unique characters in string
        let str = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent eget lacinia leo, et vulputate lorem. Quisque ullamcorper, ex quis condimentum congue, enim risus venenatis augue, sit amet auctor augue elit eu mi. Vestibulum tempus dignissim mi non volutpat. Cras vehicula pellentesque neque et mattis. Aliquam a consequat turpis, non molestie tortor. Nulla egestas tortor nibh, eget vestibulum nibh sagittis pharetra. Integer tincidunt dapibus nisi ut accumsan. Maecenas eleifend libero sit amet imperdiet ornare. Morbi dignissim commodo nibh, eu maximus ex congue at. Vestibulum vitae fermentum massa. Vestibulum ac nunc efficitur, hendrerit sem sed, consequat quam. Suspendisse magna sapien, fermentum eget nunc eget, auctor ullamcorper tellus. Nunc at volutpat orci. Morbi venenatis, quam at fermentum tincidunt, est lacus lobortis eros, sit amet faucibus ex diam eget massa. Morbi semper tempor mauris, at lobortis augue feugiat sed."

        countAndPrint(str: str)

        // MARK: ex. #2 - unwrap NetworkResult inside callback function to fit these cases:
        callback(result: NetworkResult(error: "Critical error", resultData: nil))
        callback(result: NetworkResult(error: nil, resultData: "Some data was received"))
        callback(result: NetworkResult(error: nil, resultData: nil))
    }
}

// MARK: ex. #1 - Count Characters
extension DRPracticalTestController {

    private func countAndPrint(str: String) {
        // TODO: 1. count occurrences of unique characters

        // TODO: 2. print result

        // TODO: 3. write test to be sure count is right
    }
}

// MARK: ex. #1 - unwrap NetworkResult
extension DRPracticalTestController {

    public struct NetworkResult {
        let error: String?
        let resultData: String?
    }

    // called on network finished
    func callback(result: NetworkResult) {
        // TODO: unwrap result

    }
}

