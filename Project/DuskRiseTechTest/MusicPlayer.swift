//
//  MusicPlayer.swift
//  DuskRiseTechTest
//
//  Created by Sattar Falahati on 20/10/21.
//

import SwiftUI

struct MusicPlayer: View {
    
    var url: String
    
    @State private var presentPlayer = false

    var body: some View {
        WebView(url: url)
    }
}

struct MusicPlayer_Previews: PreviewProvider {
    static var previews: some View {
        MusicPlayer(url: "")
    }
}
